
const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const port = 8081;
const bodyParser = require("body-parser");

app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/exercise_routine", { useNewUrlParser: true });
mongoose.Promise = Promise;
var db = mongoose.connection;

app.use(express.static(__dirname + "/build"));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/api/routines', (req, resp) => {
  db.collection('routine').find({}).toArray((error, routines) => {
    resp.json({ routines });
  });
});

app.get('/api/exercises', (req, resp) => {
  db.collection('exercise').find({}).toArray((error, exercises) => {
    resp.json({ exercises });
  });
});

app.post('/api/signup', (req, resp) => {
  console.log(req.body);
  db.collection('signup').insert(req.body).then((error, exercises) => {
    console.log(exercises);
    resp.json({ exercises });
  });
});

app.get('/api/login', (req, resp) => {
  db.collection('signup').find({}).toArray((error, exercises) => {
    resp.json(exercises );
  });
});

// app.get('/api/exercises/:exercise', (req, resp) => {
//   exercise =  req.params.exercise;
//   console.log('exercise' + exercise);
//   // db.collection('exercise').find({}).toArray((error, exercises) => {
//   //   resp.json({ exercises });
//   // });
// });

app.get('*', function (req, res) {
  res.sendFile(__dirname + "/build/index.html");
});

app.listen(port, () => console.log(`server is running on localhost ${port}`));