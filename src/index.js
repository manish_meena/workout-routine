import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import './styles/main.css';
import './styles/header.css';
import './styles/banner.css';
import './styles/footer.css';
import './styles/registration.css';
import './styles/routine.css';
import './styles/about.css';
import './styles/login.css';
import './styles/exercise.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';

import Registration from "./components/Registration";
import Login from "./components/Login";
import Contact from "./components/Contact";
import SignUp from "./components/Signup";
import Exercise from "./components/Exercise";
import Home from './components/Home';
import Routine from './components/Routine';
import ViewExercise from './components/ViewExercise';
import Profile from './components/Profile';
import About from './components/About';
import PageNotFound from './components/PageNotFound';
import Header from './view/Header';
import store from './store/Store';
import ViewRoutines from './components/ViewRoutines';

import '../node_modules/font-awesome/css/font-awesome.min.css';

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Header />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/profile" component={Profile} />
                    <Route exact path="/routines" component={Routine} />
                    <Route exact path='/registration' component={Registration} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/signup' component={SignUp} />
                    <Route exact path='/about' component={About} />
                    <Route exact path='/exercises' component={Exercise} />
                    <Route exact path='/exercises/:exercise' component={ViewExercise} />
                    <Route exact path='/routines/:routine' component={ViewRoutines} />
                    <Route exact path="/contact" component={Contact} />
                    <Route path="*" component={PageNotFound} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
