const rootReducer = (state = [], action) => {
    // console.log(action.payload)
    switch (action.type) {
      case 'ROUTINE':
        state = []
        return [
          ...state,
          action.payload
        ]
      case 'EXERCISE':
        state = []
        return [
          ...state,
          action.payload
        ]
      default:
        return state
    }
  }
  export default rootReducer;