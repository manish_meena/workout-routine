export function getData() {
    console.log("getdata");
    return dispatch => {
        fetch("/api/routines",{
            headers :{
                "content-type" : "application/json"
            }
        }).then(res => res.json())
        .then(data =>{
            console.log(data);
            dispatch(routineData(data))
        })
    }
}

export function getExerciseData() {
    // console.log("exercise");
    return dispatch => {
        fetch("/api/exercises",{
            headers :{
                "content-type" : "application/json"
            }
        }).then(res => res.json())
        .then(data =>{
            // console.log(data);
            dispatch(exerciseData(data))
        })
    }
}

export function routineData(data) {
    return {
      type: "ROUTINE",
      payload: data
    };
  }
  
  export function exerciseData(data) {
    return {
      type: "EXERCISE",
      payload: data
    };
  }
  