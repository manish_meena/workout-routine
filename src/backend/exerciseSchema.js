var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var exerciseSchema = new Schema({
  
 exerciseId : String,
 exerciseName : String,
 duration : Number,
 rest : Number,
 image : String,
 description : String,
 steps : String

});

var Exercise = mongoose.model('Exercise', exerciseSchema);


