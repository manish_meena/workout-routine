import {createStore,applyMiddleware} from '../../../../.cache/typescript/2.9/node_modules/redux'
import rootReducer from '../reducers/reducer';
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

store.subscribe(()=>{
    console.log(store.getState());
})

export default store;