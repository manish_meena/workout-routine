import React, { Component } from "react";
import { Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <footer>
        <p>&copy;2018 </p>
        <Link className="link" to="/about">
          About
        </Link>
        <Link className="link" to="/contact">
          Contact
        </Link>
      </footer>
    );
  }
}

export default Footer;
