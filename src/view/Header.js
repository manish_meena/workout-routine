import React, { Component } from "react";
import { Link } from "react-router-dom";
const MenuBar = require('../images/icons/menu.svg');

class Header extends Component {
  render() {
    function w3_open() {
      document.getElementById("mySidebar").style.display = "block";
      document.getElementById("myOverlay").style.display = "block";
  }
  function w3_close() {
      document.getElementById("mySidebar").style.display = "none";
      document.getElementById("myOverlay").style.display = "none";
  }
    return (
      <header>
        <div className="logo">
          <Link className="link" to="/">
            <h1>B-FIT</h1>
          </Link>
          <div className="w3-sidebar w3-bar-block w3-animate-left" style={{ "display": "none", "zIndex": "5" }} id="mySidebar">
            <button className="w3-bar-item w3-button w3-large" onClick={w3_close}>Close &times;</button>
              <Link className="w3-bar-item w3-button" to="/routines">Routine</Link>
              <Link className="w3-bar-item w3-button" to="/exercises">Exercise</Link>
              {/* <Link className="w3-bar-item w3-button" to="/about">About</Link> */}
              <Link className="w3-bar-item w3-button" to="/contact">Contact</Link>
            </div>
            <div className="w3-overlay w3-animate-opacity" onClick={w3_close} style={{ "cursor": "pointer" }} id="myOverlay"></div>
              <div>
                <button className="w3-button w3-white w3-xlarge" onClick={w3_open}>&#9776;</button>
              </div>
          </div>
          <div className="navigation-bar">
            <Link className="link" to="/routines">
              Routine
            </Link>
            <Link className="link" to="/exercises">
              Exercise
            </Link>
        </div>
      </header>
    );
  }
}

export default Header;
