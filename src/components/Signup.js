import React, { Component } from "react";
import banner from "../images/banner.jpg";
import Footer from "../view/Footer";
import { Route, Link } from "react-router-dom";
import GoogleLogin from "react-google-login";
import { Redirect } from "react-router-dom";
import { PostData } from "./PostData";
import swal from "sweetalert2";

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      redirect: false
    };
    this.signup = this.signup.bind(this);
  }
  signup(res, type) {
    let postData;
    if (type === "google" && res.w3.U3) {
      postData = {
        name: res.w3.ig,
        provider: type,
        email: res.w3.U3,
        provider_id: res.El,
        token: res.Zi.access_token,
        provider_pic: res.w3.Paa
      };
      console.log(postData);
    }

    if (postData) {
      PostData("signup", postData).then(result => {
        let responseJson = result;
        sessionStorage.setItem("userData", JSON.stringify(responseJson));
        this.setState({ redirect: true });
      });
    } else {
    }
  }
  signUpHandler = () => {
    let name = document.getElementById("name").value;
    let username = document.getElementById("username").value;
    let psw = document.getElementById("psw").value;
    let data = {
      email: username,
      password: psw
    };
    console.log(data);
    fetch("/api/signup", {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error))
      .then(response => console.log(response));
  };
  render() {
    if (this.state.redirect || sessionStorage.getItem("userData")) {
      // return <Redirect to={"/exercises"} />;
    }
    const responseGoogle = response => {
      console.log("google console");
      console.log(response);
      this.signup(response, "google");
    };
    return (
      <div>
        <div className="login">
          <div>
            <div className="field">
              <input className="signup-input" id="name" type="text" name="name" placeholder="Full Name" required />
            </div>
            <div className="field">
              <input className="signup-input" id="username" type="text" name="username" placeholder="Username" required />
            </div>
            <div className="field">
              <input className="signup-input" id="psw" type="password" name="password" placeholder="Password" required />
            </div>
            <div className="field">
              <input className="signup-input" type="email" name="email" placeholder="Email" required />
            </div>
            <div className="field">
              <input className="signup-input" type="text" name="mobile" placeholder="Mobile No." required />
            </div>
            <div className="field-btn">
              <Link className="link" to="/login">
                <button
                  onClick={() => {
                    swal("successfully Signed In !!!");
                    this.signUpHandler();
                  }}
                  className="signup-button"
                >
                  Signup
                </button>
              </Link>
            </div>
            <div className="field-btn">
              <GoogleLogin
                clientId="118325113534-3lr5q5b143q6s12juqgtef02e71p0bsk.apps.googleusercontent.com"
                buttonText="Signup with Google"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
              />
            </div>
            <div className="extra"></div>
          </div>
        </div>
      </div>
    );
  }
}
