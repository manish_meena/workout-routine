import React, { Component } from "react";
import banner from "../images/banner.jpg";
import Footer from "../view/Footer";
import GoogleLogin from "react-google-login";
import { PostData } from "./PostData";
import swal from "sweetalert2";
import { Redirect } from "react-router-dom";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginError: false,
      redirect: false,
      loginData: [],
      status: false
    };

    this.signup = this.signup.bind(this);
  }
  signup(res, type) {
    let postData;
    if (type === "google" && res.w3.U3) {
      postData = {
        name: res.w3.ig,
        provider: type,
        email: res.w3.U3,
        provider_id: res.El,
        token: res.Zi.access_token,
        provider_pic: res.w3.Paa
      };
      console.log(postData);
    }

    if (postData) {
      PostData("signup", postData).then(result => {
        let responseJson = result;
        sessionStorage.setItem("userData", JSON.stringify(responseJson));
        this.setState({ redirect: true });
      });
    } else {
    }
  }
  loginHandler = () => {
    this.setState({ status: false });
    let email = document.getElementById("email").value;
    let psw = document.getElementById("psw").value;
    fetch("/api/login", {
      headers: {
        "content-type": "application/json"
      }
    })
      .then(res => res.json())
      .then(loginData => {
        console.log(loginData);
        this.setState({ loginData: loginData });
      });
    for (let i = 0; i < this.state.loginData.length; i++) {
      if (
        this.state.loginData[i].email == email &&
        this.state.loginData[i].password == psw
      ) {
        this.setState({ status: true });
        break;
      }
    }
  };
  render() {
    if (this.state.redirect || sessionStorage.getItem("userData")) {
        // return <Redirect to={"/exersices"} />;
    }
    console.log(this.state.loginData);
    const responseGoogle = response => {
      console.log("google console");
      console.log(response);
      this.signup(response, "google");
    };
    {
      if (this.state.status) {
        swal("Good job!", "you success to login", "success");
        return <Redirect to={"/exercises"} />;
      } else {
        return (
          <div>
            <div className="login">
              <div>
                <div className="field">
                  <input
                    id="email"
                    type="text"
                    name="username"
                    placeholder="Username"
                    className="signup-input"
                  />
                </div>
                <div className="field">
                  <input
                    id="psw"
                    type="password"
                    name="password"
                    placeholder="Password"
                    className="signup-input"
                  />
                </div>
                <div className="field-btn">
                  <button className="signup-button" onClick={this.loginHandler} >
                    Login
                  </button>
                </div>
                <div className="field-btn">
                  <GoogleLogin
                    clientId="118325113534-3lr5q5b143q6s12juqgtef02e71p0bsk.apps.googleusercontent.com"
                    buttonText="Login with Google"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogle}
                  />
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
  }
}

export default Login;
