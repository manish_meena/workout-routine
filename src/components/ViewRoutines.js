import React, { Component } from "react";
import buzzer from "../sounds/whistle.wav";
import VoicePlayer from "../VoiceModulation/VoicePlayer";
import VoiceRecognition from "../VoiceModulation/VoiceRecognition";
import { connect } from "react-redux";
import { getData } from "../actions/action";
import Footer from "../view/Footer";

let steps;
let stepsList;
class Yoga extends Component {
  constructor(props) {
    super(props);
    this.buzzer = new Audio(buzzer);
    this.state = { secondsElapsed: 0 };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  getExerciseData = event => {
    this.props.getData();
  };
  durationPlay = () => {
    console.log(this.refs.duration.value);
    this.intervalID = setTimeout(() => this.tick(), this.refs.duration.value);
  };
  tick(e) {
    this.buzzer.play();
    clearInterval(this.incrementer);
    this.state = { secondsElapsed: 0 };
  }
  getMinutes = () => {
    return Math.floor(this.state.secondsElapsed / 60);
  };
  getSeconds = () => {
    return ("0" + (this.state.secondsElapsed % 60)).slice(-2);
  };
  handleStartClick = e => {
    let t = this;
    this.incrementer = setInterval(function() {
      t.setState({
        secondsElapsed: t.state.secondsElapsed + 1
      });
    }, 1000);
  };
  handleStopClick = () => {
    clearInterval(this.incrementer);
  };
  playBuzzer = () => {
    this.buzzer.play();
  };
  render() {
    let routine = this.props.match.params.routine;
    let obj = this.props.routines[0];
    let routineName;
    let arr;
    for (let key in obj) {
      let child = obj[key][0];
      for (let k in child) {
        if (child[k]._id === routine) {
          routineName = child[k].routineName;
          arr = child[k].routineExercises;
        }
      }
    }
    return (
      <div>
      <div className="routine-container">
        {arr.map(item => {
          stepsList = item.steps.split(" •");
          return (
            <div className="routine-content">
              <h3 className="routine-name">{item.exerciseName}</h3>
              <div className="routine-image"></div>
              <img className="routines-image" src={require("../images/" + item.image)}/>
              <div className="routine-description">{stepsList.map((item, index) => {
                return <p>{index + 1 + ". " + item}</p>;
              })}
              </div>
              <div className="routine-timer">
                <h1 ref="stopWatch" className="">
                  {this.getMinutes()}:{this.getSeconds()}
                </h1>
                <input
                  type="text"
                  ref="duration"
                  value={item.duration}
                  style={{ display: "none" }}
                />
                <button
                  className="routine-btn"
                  onClick={event => {
                    this.handleStartClick(event);
                    this.durationPlay(event);
                  }}
                >
                  Play
                </button>
              </div>
            </div>
          );
        })}
      </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { routines: state };
}

export default connect(
  mapStateToProps,
  { getData }
)(Yoga);
