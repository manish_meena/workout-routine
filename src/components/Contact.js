import React from "react";

export default class Form extends React.Component {
    state = {
        firstName: "",
        lastName: "",
        phonenumber: "",
        email: "",
        message: ""
    };

    change = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    onSubmit = e => {
        e.preventDefault();
        console.log(this.state)
        this.setState({
            firstName: "",
            lastName: "",
            phonenumber: "",
            email: "",
            message: ""
        });
    };
  

    render() {
        let subButton ={
        
        }
        return (
            <div >
                <h2 className="contact-header1-tag"> Contact Us </h2>
                <div className="contact-form-contianer">
                    <form>
                        <div>
                            <label>  First name:<span style={{ color: "red" }}> *</span>
                            <br/>
                             <input
                                className="contact-input-tag"
                                name="firstName"
                                placeholder="First name"
                                value={this.state.firstName}
                                onChange={e => this.change(e)}
                            />
                            </label>
                        </div>
                        <br />
                        <div>
                            <label> Last name: <span style={{ color: "red" }}> *</span>
                            <br/>
                                <input
                                    className="contact-input-tag"
                                    name="lastName"
                                    placeholder="Last name"
                                    value={this.state.lastName}
                                    onChange={e => this.change(e)}
                                />
                            </label>
                        </div>
                        <br />
                        <div>
                            <label> Phone no.:
                            <br/>
                                <input
                                    className="contact-input-tag"
                                    name="phonenumber"
                                    placeholder="phone no."
                                    value={this.state.phonenumber}
                                    onChange={e => this.change(e)}
                                /> </label>
                        </div>
                        <br />
                        <div>
                            <label> Email id: <span style={{ color: "red" }}> *</span>
                            <br/>
                                <input
                                    className="contact-input-tag"
                                    name="email"
                                    placeholder="Email"
                                    value={this.state.email}
                                    onChange={e => this.change(e)}
                                />
                            </label>
                        </div>
                        <br />
                        <div>
                            <label> Message : <span style={{ color: "red" }}> *</span>
                            <br/>
                                <textarea
                                    cols={40} rows={10}
                                    className="contact-input-tag"
                                    name="message"
                                    type="text"
                                    placeholder="message"
                                    value={this.state.message}
                                    onChange={e => this.change(e)}
                                />
                            </label>
                        </div>
                        <br />
                        <button className="contact-submit-button" onClick={e => this.onSubmit(e)}>Submit</button>
                    </form>
                </div>
            </div>
        );
    }
}