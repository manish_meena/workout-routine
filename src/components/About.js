import React, { Component } from "react";

class About extends Component {
  render() {
    return (
      <div className="about-container">
        <div className="about-content">
          <h1 className="about-content-head">
            Our goal is to make health and fitness attainable, affordable and
            approachable.
          </h1>
        </div>
        <div className="about-text">
          <p className="about-text-head">
            Created to help you live a better, happier, healthier life.
          </p>
          <p className="about-text-content">
            We believe fitness should be accessible to everyone, everywhere,
            regardless of income level or access to a gym. That's why we offer
            hundreds of free, full-length workout videos, the most affordable
            and effective workout programs on the web, meal plans, and helpful
            health, nutrition and fitness information.
          </p>
        </div>
        <div className="aboutImg">{/* <img src={AboutImg}/> */}</div>
        <div className="about-text2">
          <p className="about-text2-head">
            We believe in unbiased, gimmick-free, research-backed information
          </p>
          <p className="about-text2-content">
            The only thing we endorse is working out for a strong, healthy body.
            As a business, we believe good things happen when you put people
            before profit.
          </p>
        </div>
      </div>
    );
  }
}

export default About;
