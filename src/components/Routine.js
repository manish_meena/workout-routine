import React, { Component } from 'react'
import YogaBanner from '../images/yoga/yoga.jpg';
import PilateBanner from '../images/pilates/pilates.jpg';
import WarmupBanner from '../images/warmup/warmup.jpg';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from "react-redux";
import { getData } from "../actions/action";
import Footer from '../view/Footer';

class Routine extends Component {
  componentDidMount() {
    this.props.getData();
    window.scrollTo(0, 0)
  }
  getExerciseData = event => {
    this.props.getData();
  };
  render() {
    {this.props.routines}
    return (
      <div>
      <div className="routine-container">
        <div className="routine-content">
          <h1>Yoga</h1>
          <Link to="/routines/yoga"><img src={YogaBanner} /></Link>
        </div>
        <div className="routine-content">
          <h1>Pilates</h1>
          <Link to="/routines/pilates"><img src={PilateBanner} /></Link>
        </div>
        <div className="routine-content">
          <h1>Warm Up</h1>
          <Link to="/routines/warmup"><img src={WarmupBanner} /></Link>
        </div>
      </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { routines: state };
}

export default connect(
  mapStateToProps,
  { getData }
)(Routine);