import React, { Component } from "react";
import buzzer from "../sounds/whistle.wav";
import VoicePlayer from "../VoiceModulation/VoicePlayer";
import VoiceRecognition from "../VoiceModulation/VoiceRecognition";
import { connect } from "react-redux";
import { getData } from "../actions/action";
import Footer from "../view/Footer";

let steps;
let stepsList;
var status = false;
class ViewExercise extends Component {
  componentWillMount() {
    window.scrollTo(0, 0);
  }
  constructor(props) {
    super(props);
    this.buzzer = new Audio(buzzer);
    this.state = { secondsElapsed: 0 };
    // this.state = { play: false };
  }
  durationPlay = () => {
    this.intervalID = setTimeout(() => this.tick(), this.refs.duration.value);
  };
  tick(e) {
    this.buzzer.play();
    clearInterval(this.incrementer);
    this.state = { secondsElapsed: 0 };
  }
  getMinutes = () => {
    return Math.floor(this.state.secondsElapsed / 60);
  };
  getSeconds = () => {
    return ("0" + (this.state.secondsElapsed % 60)).slice(-2);
  };
  handleStartClick = e => {
    let t = this;
    this.incrementer = setInterval(function() {
      t.setState({
        secondsElapsed: t.state.secondsElapsed + 1
      });
    }, 1000);
  };
  handleStopClick = () => {
    clearInterval(this.incrementer);
  };
  playBuzzer = () => {
    this.buzzer.play();
  };
  render() {
    let exercise = this.props.match.params.exercise;
    let obj = this.props.exercises[0];
    let exerciseName, duration, image;
    for (let key in obj) {
      let child = obj[key][0];
      for (let k in child) {
        if (child[k]._id === exercise) {
          exerciseName = child[k].exerciseName;
          steps = child[k].steps;
          duration = child[k].duration;
          image = child[k].image;
        }
      }
    }
    stepsList = steps.split(" •");
    let playSteps = () => {
      return (
        <div>
          {stepsList.map((steps)=>{
           return(
             <VoicePlayer play text={steps} />
           )
         })}
        </div>
      );
    };
    let someMethod = () => {
      this.setState({
        play: true
      });
    };
    return (
      <div>
        <div className="exercise">
          <div className="exercise-title">
            <h3>{exerciseName}</h3>
          </div>
          <div className="exercise-banner">
            <img src={require("../images/" + image)} />
          </div>
          <div className="exercise-description">
            <p className="steps-header">Steps to perform exercise :</p>
            <p className="steps-text">
              {stepsList.map((item, index) => {
                return <p>{index + 1 + ". " + item}</p>;
              })}
            </p>
            <p className="steps-text">{duration}</p>
          <div className="exercise-button">
            <button onClick={someMethod}>Steps</button>
            {this.state.play ? playSteps() : ""}
          </div>
          </div>
          <div className="exercise-timer">
            <h1 ref="stopWatch">
              {this.getMinutes()}:{this.getSeconds()}
            </h1>
            <input
              value={duration}
              style={{ display: "none" }}
              type="text"
              ref="duration"
            />
            <button className="exercise-btn"
              onClick={event => {
                this.handleStartClick(event);
                this.durationPlay(event);
              }}
            >
              Start
            </button>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { exercises: state };
}

export default connect(
  mapStateToProps,
  { getData }
)(ViewExercise);
