import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import banner from '../images/banner.jpg';
import Footer from '../view/Footer';

class Registration extends Component {
    render() {
        return (
            <div className="register">
                <div className="banner">
                    <img src={banner} />
                    <div className="inner-banner"></div>
                    <div className="registration">
                        <Link className="link" to='/login'><button>Login</button></Link>
                        <Link className="link" to='/signup'><button>Signup</button></Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default Registration;