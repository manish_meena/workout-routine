import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import Registration from './Registration';
import banner from '../images/banner.jpg';
import Footer from '../view/Footer';


class Home extends Component {
  // googlAuth = () => {
  //   console.log("ok")
  // }
  render() {
    return (
      <div>
      <div className="container">
        <div className="banner">
          <img src={banner} />
          <div className="inner-banner"></div>
          <div className="heading">
            <p>Don't find time to exercise,</p>
            <p><span>make</span> time to exercise</p>
          </div>
          <div className="get-started">
            <Link to='/registration'><button>Get Started</button></Link>
          </div>
        </div>
      </div>
      </div>
    )
  }
}

export default Home;