import React,{ Component } from 'react';

class PageNotFound extends Component {
   render() {
       return (
           <div className="pageNotFound">
               <h1>Page Not Found</h1>
           </div>
        );
   }
}

export default PageNotFound;