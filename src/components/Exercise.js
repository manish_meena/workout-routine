import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { connect } from "react-redux";
import { getExerciseData } from "../actions/action";
import Footer from "../view/Footer";

class Exercise extends Component {
  componentWillMount() {
    this.getExerciseData();
    window.scrollTo(0, 0)
  }
  componentDidMount() {
    this.getExerciseData();
  }
  getExerciseData = event => {
    this.props.getExerciseData();
  };
  render() {
    let obj = this.props.routines[0];
    let exerciseList = [];
    for (let key in obj) {
      let child = obj[key][0];
      for (let k in child) {
        exerciseList.push(child[k]);
      }
    }
    exerciseList.pop();
    return (
      <div>
      <div className="exercise-container">
        {exerciseList.map(item => {
          return (
            <div className="exercise-content">
              <h3>{item.exerciseName}</h3>
              <Link to={"/exercises/" + item._id}>
                <img className="yoga-image"
                  src={require("../images/" + item.image)} />
              </Link>
            </div>
          );
        })}
      </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { routines: state };
}

export default connect(
  mapStateToProps,
  { getExerciseData }
)(Exercise);